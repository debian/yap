/* fibfib(N,R1,R2) : R1=fibonacci(N-1) && R2=fibonacci(N) */
fibfib(1,1,1).
fibfib(N,R1,R2) :-
	N > 1, M is N-1,
	fibfib(M,S1,S2),
	R1 is S2, R2 is S1+S2.

/* fib(N,R) : R=fibonacci(N) */
fib(N,R) :- N>0, fibfib(N,R1,R), write(R), nl.
