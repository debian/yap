Document: yap
Title: YAP Prolog User's Manual
Author: Vitor Santos Costa, Luis Damas, Rogerio Reis, and Ruben Azevedo
Abstract: This manual documents the YAP Prolog System, a high-performance
 Prolog compiler developed at LIACC, Universidade do Porto.
Section: Programming/Prolog

Format: HTML
Index: /usr/share/doc/yap/yap.html
Files: /usr/share/doc/yap/yap.html
